import os
import sys
import shutil
import logging
from time import sleep
import numpy as np
Period = "300"

import helper.config
import helper.requestcsv
import helper.tradingfunctions
import helper.sftp

import CLASS.DATABASE

# Init Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Train.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Init Config
conf = helper.config.initconfig()

# Init Database
Database = CLASS.DATABASE.DATABASE()
Database.connect()
Database.setup()

# Init the Parameters
CurrencyPair = sys.argv[1]
stoploss = np.arange(0.01,0.2,0.01)
takeprofit = np.arange(0.01,0.2,0.01)
list_nPCs = range(5,50) # [5, 10, 15, 20, 25, 30, 35, 40]

# Creating subfolders if they don't exist ...
if not os.path.exists('./Data'):
        os.makedirs('./Data')
if not os.path.exists('./Models'):
        os.makedirs('./Models')
if not os.path.exists('./Results'):
        os.makedirs('./Results')

# Delete all folders at the beginning
if os.path.exists('./Data/CurrencyPair{}/Period{}'.format(CurrencyPair, Period)):
    shutil.rmtree('./Data/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
# if os.path.exists('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period)):
#     shutil.rmtree('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
if os.path.exists('./Results/CurrencyPair{}/Period{}'.format(CurrencyPair, Period)):
    shutil.rmtree('./Results/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))

# Init Trainer Status
TrainerStatus = {}
TrainerStatus['CurrencyPair'] = CurrencyPair
TrainerStatus['Status'] = "Started"
Database.insertTrainer(TrainerStatus)

# Start to Request the CSV Data
try:
    helper.requestcsv.request(CurrencyPair,Period)
except:
    helper.requestcsv.request(CurrencyPair,Period)
logging.info("Finish requestcsv")
TrainerStatus['Status'] = "Finish RequestCSV"
Database.insertTrainer(TrainerStatus)

# Start to Sort the CSV Data
helper.tradingfunctions.sortdata(CurrencyPair,Period)
logging.info("Finish sortdata")
TrainerStatus['Status'] = "Finish Sortdata"
Database.insertTrainer(TrainerStatus)

# Init the loop counter
count = 0
maxcount = len(stoploss)*len(takeprofit)*len(list_nPCs)
TrainerStatus['maxcount'] = maxcount

# Start the loop for stoploss and takeprofit
for stop in stoploss:
        for take in takeprofit:

            # Compute
            logging.info("Stoploss: {} Takeprofit: {}".format(stop,takeprofit))
            helper.tradingfunctions.compute(CurrencyPair,Period,stop,take)
            status = "Finish Compute {}/{} Stoploss: {} Takeprofit: {}".format(count,maxcount,stop,take)
            logging.info(status)

            # Prepair train
            helper.tradingfunctions.prepairtrain(Database,CurrencyPair,Period,stop,take)
            status = "Finish Prepairtrain {}/{} Stoploss: {} Takeprofit: {}".format(count,maxcount,stop,take)
            logging.info(status)
            for nPCs in list_nPCs:
                if not (Database.requestModels(CurrencyPair,stop,take,nPCs)) or not helper.sftp.checkdir(CurrencyPair, Period, nPCs, stop, take):
                    # Train
                    helper.tradingfunctions.train(Database, CurrencyPair,Period,stop,take,nPCs)
                    status = "Finish Train {}/{} Stoploss: {} Takeprofit: {} nPCs: {}".format(count,maxcount,stop,take,nPCs)
                    logging.info(status)

                    # Test
                    helper.tradingfunctions.test(Database, CurrencyPair,Period,stop,take,nPCs)
                    status = "Finish Test {}/{} Stoploss: {} Takeprofit: {} nPCs: {}".format(count,maxcount,stop,take,nPCs)
                    logging.info(status)

                    # helper.tradingfunctions.ROIperform(CurrencyPair,Period,stop,take,Database)
                    # status = "Finish ROI {}/{} Stoploss: {} Takeprofit: {}".format(count,maxcount,stop,take)
                    # logging.info(status)
                    # TrainerStatus['Status'] = status
                    # Database.insertTrainer(TrainerStatus)

                    # Search the Best
                    helper.tradingfunctions.search_the_best(Database,CurrencyPair,Period,stop,take,nPCs)

                else:
                    print("to next {}/{}".format(count,maxcount,stop,take,nPCs))
                    #sleep(10)
                status = "Finish {}/{}\n Stoploss: {} Takeprofit: {} nPCs: {}".format(count,maxcount,stop,take,nPCs)
                logging.info(status)
                TrainerStatus['Status'] = status
                TrainerStatus['count'] = count
                TrainerStatus['stoploss'] = stop
                TrainerStatus['takeprofit'] = take
                TrainerStatus['nPCs'] = nPCs
                Database.insertTrainer(TrainerStatus)
                count = count + 1

TrainerStatus['Status'] = "Finish Train"
Database.insertTrainer(TrainerStatus)

# Delete all folders at the end
shutil.rmtree('./Data/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
shutil.rmtree('./Results/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
shutil.rmtree('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))