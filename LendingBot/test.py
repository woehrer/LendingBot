import helper.sftp

CurrencyPair = "TRX_JST"
Period = 300
nPCs = 5
stoploss = 0.01
takeprofit = stoploss

localfilePath = './Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit)
helper.sftp.checkdir(CurrencyPair,Period, nPCs, stoploss, takeprofit)

helper.sftp.readfile('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair,Period),CurrencyPair,Period)
