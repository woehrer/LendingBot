import CLASS.POLONIEXAPI
import helper.config
import helper.telegramsend
import helper.requestBTCEUR
import CLASS.DATABASE
import helper.lendingfunctions
from datetime import datetime,timedelta
import os
import time
import logging

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Lending.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

conf = helper.config.initconfig()

if conf['Database'] == "True":
    Database = CLASS.DATABASE.DATABASE()
    Database.connect()
    Database.setup()

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def print_lending():
    AvailableBalancedata = Poloniexapi.availableAccountBalances
    if 'lending' in AvailableBalancedata:
        for key in AvailableBalancedata["lending"]:
            if float(AvailableBalancedata["lending"][key]) > 0.0:
                print("You have " + AvailableBalancedata["lending"][key] + " " + str(key) + " available")
    else:
        print("You have nothing available")

def print_activ_loans():
    ActiveLoans = Poloniexapi.activeLoans
    if ActiveLoans != []:
        print("Active Loans")
        for key in ActiveLoans["provided"]:
            print(key["currency"] + ": Rate: " + key["rate"] + "% Amount: " + key ["amount"])
    else:
        print("No Active Loans")

def print_open_loans():
    OpenLoans = Poloniexapi.OpenLoanOffers
    if OpenLoans != []:
        print("Open Loans")
        for value1 in OpenLoans:
            for value2 in OpenLoans[value1]:
                print(value1 + ": Rate: " + value2["rate"] + "% Amount: " + value2 ["amount"])
    else:
        print("No Open Loans")

def transfer_trx():
    AvailableBalancedata = Poloniexapi.availableAccountBalances
    if 'TRX' not in AvailableBalancedata['exchange'] or float(AvailableBalancedata['exchange']['TRX']) < 90.0:
        if 'TRX' in AvailableBalancedata['lending'] and float(AvailableBalancedata['lending']['TRX']) > 100:
            Poloniexapi.transferBalance('TRX',50,'lending','exchange')
            helper.telegramsend.send("50 TRX Tranfer from Lending to Exchange")

def print_screen():
    Tickerdata = Poloniexapi.ticker
    BTCEUR = helper.requestBTCEUR.request()
    cls()
    print("LENDING BOT")
    print("USDT_BTC: " + Tickerdata["USDT_BTC"]["last"])
    print("")
    print_lending()
    print("")
    print_activ_loans()
    print("")
    print_open_loans()
    print("")
    print(str(Poloniexapi.completeBalancesSum()) + " BTC")
    print(str(Poloniexapi.completeBalancesSum()*BTCEUR) + " €")


def check():
    AvailableBalancedata = Poloniexapi.availableAccountBalances
    ActiveLoans = Poloniexapi.activeLoans
    time.sleep(1)
    HistoryLoans = Poloniexapi.LendingHistory
    HistoryTrade = Poloniexapi.TradeHistory
    time.sleep(1)
    OpenLoans = Poloniexapi.OpenLoanOffers
    transfer_trx()
    helper.lendingfunctions.lendcheck(AvailableBalancedata, OpenLoans)
    
    if conf['Database'] == "True":
        for key in ActiveLoans["provided"]:
            Text = Database.insertActiveLoan(key)
            if len(Text) > 1 :
                helper.telegramsend.send(Text)
        Text = Database.insertHistoryLoan(HistoryLoans)
        if len(Text) > 1 :
            helper.telegramsend.send(Text)
        Text = Database.insertHistoryTrade(HistoryTrade)
        if len(Text) > 1 :
            helper.telegramsend.send(Text)
        Database.insertTicker(Poloniexapi.ticker)

while True:
    try:
        conf = helper.config.initconfig()
        Poloniexapi.request()
        print_screen()
        check()

    except TypeError:
        helper.telegramsend.send("TypeError CRASHED!!!!")
        print("TypeError CRASHED!!!!")
    except AttributeError:
        helper.telegramsend.send("AttributeError CRASHED!!!!")
        print("AttributeError CRASHED!!!!")
    except Exception as e:
        helper.telegramsend.send("Error '{0}' occurred. Arguments {1}.".format(e, e.args))
        print("Error '{0}' occurred. Arguments {1}.".format(e, e.args))
    finally:
        time.sleep(10)