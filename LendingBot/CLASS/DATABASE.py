from pyArango.connection import Connection
import pyArango
import sys
import datetime
import time
import helper.config
import logging

class DATABASE():

    def connect(self):
        try:
            conf = helper.config.initconfig()
            self.conn = Connection(arangoURL=conf['dburl'], username=conf['dbuser'], password=conf['dbpw'])
        except ZeroDivisionError:
            print("ZeroficisionError")
        except TypeError:
            errortext = "TypeError: Connection to Database failed\n " + str(sys.exc_info())
            print(errortext)

    def setup(self):
        if not self.conn.hasDatabase(name='Lending'):
            db = self.conn.createDatabase(name='Lending')
        db = self.conn['Lending']

        if not db.hasCollection(name='Ticker'):
            db.createCollection(name='Ticker')

        if not db.hasCollection(name='ActiveLoans'):
            db.createCollection(name='ActiveLoans')

        if not db.hasCollection(name='LoansHistory'):
            db.createCollection(name='LoansHistory')

        if not db.hasCollection(name='TradeHistory'):
            db.createCollection(name='TradeHistory')

        if not db.hasCollection(name='Trainer'):
            db.createCollection(name='Trainer')

        if not db.hasCollection(name='Trader'):
            db.createCollection(name='Trader')

        if not db.hasCollection(name='Models'):
            db.createCollection(name='Models')

        if not db.hasCollection(name='Pickle'):
            db.createCollection(name='Pickle')

        if not db.hasCollection(name='Trade'):
            db.createCollection(name='Trade')

    def insertTicker(self,data):
        db = self.conn['Lending']
        tickerCollection = db['Ticker']
        tickerDoc = tickerCollection.createDocument()
        for keys in data:
            tickerDoc[keys] = data[keys]
        tickerDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        tickerDoc['TIMESTAMP'] = int(time.time())
        tickerDoc.save()

    def insertActiveLoan(self,data):
        Text = ""
        db = self.conn['Lending']
        aql = "FOR x IN ActiveLoans FILTER x.id == " + str(data['id']) + " RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            Text = "New Loan: \nID: " + str(data['id']) + "\nCurrency: " + data['currency'] + "\nRate: " + str(data['rate']) + "\nAmount: " + str(data['amount'])  + "\nDuration: " + str(data['duration'])
            ActiveLoanCollection = db['ActiveLoans']
            ActiveLoanDoc = ActiveLoanCollection.createDocument()
            for keys in data:
                ActiveLoanDoc[keys] = data[keys]
            ActiveLoanDoc.save()
        return Text

    def insertHistoryLoan(self,data):
        Text = ""
        db = self.conn['Lending']
        for datakey in data:
            aql = "FOR x IN LoansHistory FILTER x.id == " + str(datakey['id']) + " RETURN x"
            queryResult = db.AQLQuery(aql, rawResults=True)
            if len(queryResult) == 0:
                Text = Text + "New History Loan: \nID: " + str(datakey['id']) + "\nCurrency: " + datakey['currency'] + "\nRate: " + str(datakey['rate']) + "\nAmount: " + str(datakey['amount']) + "\nDuration: " + str(datakey['duration']) +  "\nEarned: " + str(datakey['earned'] + "\n\n")
                LoansHistoryCollection = db['LoansHistory']
                LoansHistoryDoc = LoansHistoryCollection.createDocument()
                for keys in datakey:
                    LoansHistoryDoc[keys] = datakey[keys]
                LoansHistoryDoc.save()
        return Text

    def insertHistoryTrade(self,data):
        Text = ""
        db = self.conn['Lending']
        for datakey in data:
            i = 0
            while i < len(data[datakey]):
                x = data[datakey][i]
                aql = "FOR x IN TradeHistory FILTER x.DATA.tradeID == '" + str(x['tradeID']) + "' RETURN x"
                queryResult = db.AQLQuery(aql, rawResults=True)
                if len(queryResult) == 0:
                    Text = Text + "New History Trade: \nID: " + str(x['tradeID']) + "\nCurrency: " + datakey + "\nRate: " + str(x['rate']) + "\nAmount: " + str(x['amount']) + "\nFee: " + str(x['fee'] + "\nType:" + x['type'] + "\n\n")
                    TradeHistoryCollection = db['TradeHistory']
                    HistoryLoanDoc = TradeHistoryCollection.createDocument()
                    HistoryLoanDoc['PAIR'] = datakey
                    HistoryLoanDoc['DATA'] = x
                    HistoryLoanDoc.save()
                i+=1
        return Text

    def requestTicker(self,time,Pair):
        db = self.conn['Lending']
        aql = "FOR x IN Ticker FILTER x.TIMESTAMP >= " + str(time) + " RETURN { currency: x." + Pair + ", date: x.date }"
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult
    
    def requestLastPrice(self,Pair):
        db = self.conn['Lending']
        aql = "FOR x IN Ticker SORT x.TIMESTAMP DESC LIMIT 1 RETURN {last: x." + Pair + ".last, date: x.date}"
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult

    def insertTrainer(self,data):
        db = self.conn['Lending']
        aql = "FOR x IN Trainer FILTER x.CurrencyPair == '" + data['CurrencyPair'] + "' REMOVE x in Trainer"
        db.AQLQuery(aql, rawResults=True)
        TrainerCollection = db['Trainer']
        TrainerDoc = TrainerCollection.createDocument()
        for keys in data:
            TrainerDoc[keys] = data[keys]
        TrainerDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        TrainerDoc['TIMESTAMP'] = int(time.time())
        TrainerDoc.save()

    def requestTrainer(self,CurrencyPair):
        db = self.conn['Lending']
        aql = "FOR x IN Trainer FILTER x.CurrencyPair == '" + CurrencyPair + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult)>0:
            return queryResult[0]
        return False

    def insertTrader(self,data):
        db = self.conn['Lending']
        aql = "FOR x IN Trader FILTER x.CurrencyPair == '" + data['CurrencyPair'] + "'\
                                && x.Period == " + str(data['Period']) + "\
                                && x.index == " + str(data['index']) + "\
                                     REMOVE x in Trader"
        db.AQLQuery(aql, rawResults=True)
        TraderCollection = db['Trader']
        TraderDoc = TraderCollection.createDocument()
        for keys in data:
            TraderDoc[keys] = data[keys]
        TraderDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        TraderDoc['TIMESTAMP'] = int(time.time())
        TraderDoc.save()

    def requestTrader(self,CurrencyPair,Period):
        db = self.conn['Lending']
        aql = "FOR x IN Trader FILTER x.CurrencyPair == '" + CurrencyPair + "'\
                                    && x.Period == " + str(Period) + "\
                                     RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult

    def insertModels(self,data):
        db = self.conn['Lending']
        aql = "FOR x IN Models \
            FILTER x.CurrencyPair == '" + data['CurrencyPair'] + "' \
            && x.stoploss == " + str(data['stoploss']) + "\
            && x.takeprofit == " + str(data['takeprofit']) + " \
            && x.nPCs == " + str(data['nPCs']) + " \
            REMOVE x in Models"
        db.AQLQuery(aql, rawResults=True)
        ModelsCollection = db['Models']
        ModelsDoc = ModelsCollection.createDocument()
        for keys in data:
            ModelsDoc[keys] = data[keys]
        ModelsDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        ModelsDoc['TIMESTAMP'] = int(time.time())
        ModelsDoc.save()

    def requestModels(self,CurrencyPair,stoploss,takeprofit,nPCs):
        db = self.conn['Lending']
        aql = "For x in Models \
                Filter x.CurrencyPair == '" + str(CurrencyPair) + "' \
                && x.stoploss == " + str(stoploss) + "\
                && x.takeprofit == " + str(takeprofit) + " \
                && x.nPCs == " + str(nPCs) + " \
                && x.TIMESTAMP > " + str(time.time() - 2592000.0) + " \
                && x.finish == True \
                Return x"
        try:
            queryResult = db.AQLQuery(aql, rawResults=True)
        except:
            time.sleep(5)
            queryResult = db.AQLQuery(aql, rawResults=True)

        if len(queryResult) >0:
            return queryResult[0]
        return False

    def addtoModels(self,data):
        db = self.conn['Lending']
        aql = "For x in Models \
                Filter x.CurrencyPair == '" + data['CurrencyPair'] + "' \
                && x.stoploss == " + str(data['stoploss']) + "\
                && x.takeprofit == " + str(data['takeprofit']) + " \
                && x.nPCs == " + str(data['nPCs']) + " \
                Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        ModelsCollection = db['Models']
        ModelsDoc = ModelsCollection.createDocument()
        for keys in queryResult[0]:
            ModelsDoc[keys] = queryResult[0][keys]
        for keys in data:
            ModelsDoc[keys] = data[keys]
        ModelsDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        ModelsDoc['TIMESTAMP'] = int(time.time())
        try:
            ModelsDoc.save()
        except:
            print(ModelsDoc)
            time.sleep(10)
            ModelsDoc.save()

    def requestBest(self,CurrencyPair):
        db = self.conn['Lending']
        aql = "For x in Models \
                Filter x.CurrencyPair == '" + CurrencyPair + "' \
                && x.ROI > 2 \
                && x.accuracies > 0.85 \
                && x.finish == True \
                SORT x.ROI DESC \
                Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult

    def insertTrade(self,data):
        db = self.conn['Lending']
        TradeCollection = db['Trade']
        TradeDoc = TradeCollection.createDocument()
        for keys in data:
            TradeDoc[keys] = data[keys]
        TradeDoc['date'] = datetime.datetime.strftime(datetime.datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
        TradeDoc['TIMESTAMP'] = int(time.time())
        TradeDoc.save()

    def requestTrade(self):
        db = self.conn['Lending']
        aql = "For x in Trade\
                Filter x.Type == 'buy'\
                Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult

    def sellTrade(self,Trade):
        db = self.conn['Lending']
        aql = "For x in Trade\
                Filter x._key == '" + str(Trade['_key']) + "'\
                Remove x in Trade"
        print(aql)
        queryResult = db.AQLQuery(aql, rawResults=True)