import helper.config
import time
import re
from poloniex import Poloniex
import poloniex

import statistics

class POLONIEXAPI():

    def init(self):
        conf = helper.config.initconfig()
        self.key = conf["api_key"]
        self.secret = conf["secret"]
        self.polo = Poloniex()
        self.polo.key = self.key
        self.polo.secret = self.secret
        time.sleep(1)

    def request(self):
        self.init()
        self.ticker = self.polo('returnTicker')
        self.completeBalances = self.polo.returnCompleteBalances()
        time.sleep(1)
        self.availableAccountBalances = self.polo.returnAvailableAccountBalances()
        self.activeLoans = self.polo.returnActiveLoans()
        time.sleep(1)
        self.LendingHistory = self.polo.returnLendingHistory()
        self.OpenLoanOffers = self.polo.returnOpenLoanOffers()
        time.sleep(1)
        self.TradeHistory = self.polo.returnTradeHistory("all")
        time.sleep(1)

    def returnLoanOrders(self,currency):
        conf = helper.config.initconfig()
        self.polo = Poloniex()
        self.polo.key = conf["api_key"]
        self.polo.secret = conf["secret"]
        return self.polo.returnLoanOrders(currency=currency)

    def returnChartData(self,pair,period):
        return self.polo.returnChartData(pair, period = period, start = int(time())- period*10000)

    def avgLoanOrders(self,currency):
        data = self.returnLoanOrders(currency)
        dicti = []
        min = float(data['offers'][0]['rate'])
        for key in data['offers']:
            dicti.append(float(key['rate']))
        dicti = statistics.quantiles(dicti,n=10)
        return float(statistics.mean(dicti)),min

    def createLoanOffer(self,currency ,amount, price):
        r = self.polo.createLoanOffer(currency, amount, price, autoRenew=0)

    def returnBalancesavailable(self,currency):
        while True:
            try:
                self.availableAccountBalances = self.polo.returnAvailableAccountBalances()
            except poloniex.RequestException:
                time.sleep(15)
                continue
            except poloniex.RetryException:
                time.sleep(15)
                continue
            break
        time.sleep(1)
        if currency in self.availableAccountBalances['exchange']:
            return float(self.availableAccountBalances['exchange'][currency])
        return 0.0

    def completeBalancesSum(self):
        x = 0.0
        for btcs in self.completeBalances:
            x = x + float(self.completeBalances[btcs]['btcValue'])
        return x

    def cancelLoanOffer(self,number):
        self.polo.cancelLoanOffer(number)
        
    def transferBalance(self,currency,amount,fromAccount,toAccount):
        self.polo.transferBalance(currency,amount,fromAccount,toAccount)

    def volume(self,Pair):
        liste = self.polo.return24hVolume()
        return liste[Pair][re.split('_', Pair)[1]]
        
    def returnTickerlowest(self,pair):
        return float(self.polo.returnTicker()[pair]['lowestAsk'])

    def returnTickerhighest(self,pair):
        return float(self.polo.returnTicker()[pair]['highestBid'])