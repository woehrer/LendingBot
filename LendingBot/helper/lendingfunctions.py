import poloniex
import CLASS.POLONIEXAPI
import helper.config
import time
from datetime import datetime,timedelta

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()

BTCmin = 0.005
USDTmin = 50.0
ATOMmin = 10
TRXmin = 2500
USDCmin = 50.0

def lend_btc(Balance, OpenLoans, Poloniexapi):
    if 'BTC' in OpenLoans:
        for x in OpenLoans['BTC']:
            Poloniexapi.cancelLoanOffer(int(x['id']))
    elif float(Balance) > BTCmin:
        createLoan('BTC',Balance,BTCmin)

def lend_usdt(Balance, OpenLoans, Poloniexapi):
    if 'USDT' in OpenLoans:
        for x in OpenLoans['USDT']:
            Poloniexapi.cancelLoanOffer(int(x['id']))
    elif float(Balance) > USDTmin:
        createLoan('USDT',Balance,USDTmin)

def lend_usdc(Balance, OpenLoans, Poloniexapi):
    if 'USDC' in OpenLoans:
        for x in OpenLoans['USDC']:
            Poloniexapi.cancelLoanOffer(int(x['id']))
    elif float(Balance) > USDCmin:
        createLoan('USDC',Balance,USDCmin)

def lend_atom(Balance, OpenLoans, Poloniexapi):
    if 'ATOM' in OpenLoans:
        for x in OpenLoans['ATOM']:
            Poloniexapi.cancelLoanOffer(int(x['id']))
    elif float(Balance) > ATOMmin:
        createLoan('ATOM',Balance,ATOMmin)

def lend_trx(Balance, OpenLoans, Poloniexapi):
    if 'TRX' in OpenLoans:
        for x in OpenLoans['TRX']:
            Poloniexapi.cancelLoanOffer(int(x['id']))
    elif float(Balance) > TRXmin:
        print("TRX Lending")
        createLoan('TRX',Balance,TRXmin)

def create4loans(Currency,ammount,avg,avgmin):
    Poloniexapi.createLoanOffer(Currency, ammount,avgmin*1.1)
    Poloniexapi.createLoanOffer(Currency, ammount,avg)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*0.90)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*1.1)

def create3loans(Currency,ammount,avg):
    Poloniexapi.createLoanOffer(Currency, ammount,avg)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*0.90)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*1.1)

def create2loans(Currency,ammount,avg):
    Poloniexapi.createLoanOffer(Currency, ammount,avg)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*0.90)

def create1loan(Currency,ammount,avg):
    print("1 Loan erstellt")
    ammount = ammount*0.9999
    print(Currency,ammount,avg)
    Poloniexapi.createLoanOffer(Currency, ammount,avg*0.90)

def lendcheck(AvailableBalancedata, OpenLoans):
    Poloniexapi.init()
    if OpenLoans != []:
        for value1 in OpenLoans:
            for value2 in OpenLoans[value1]:
                present = datetime.utcnow()
                future  = datetime.strptime(value2["date"], '%Y-%m-%d %H:%M:%S')
                difference = present - future
                if difference > timedelta(hours=6):
                    Poloniexapi.cancelLoanOffer(value2['id'])
    if 'lending' in AvailableBalancedata:
        for key in AvailableBalancedata['lending']:
            lend(key, float(AvailableBalancedata['lending'][key]), OpenLoans, Poloniexapi)

def lend(Currency, Balance, OpenLoans, Poloniexapi):
    if Currency == 'BTC' and Balance > BTCmin:
        lend_btc(Balance, OpenLoans, Poloniexapi)
    if Currency == 'USDT' and Balance > USDTmin:
        lend_usdt(Balance, OpenLoans, Poloniexapi)
    if Currency == 'USDC' and Balance > USDCmin:
        lend_usdc(Balance, OpenLoans, Poloniexapi)
    if Currency == 'ATOM' and Balance > ATOMmin:
        lend_atom(Balance, OpenLoans, Poloniexapi)
    if Currency == 'TRX' and float(Balance) > TRXmin-102:
        lend_trx(Balance, OpenLoans, Poloniexapi)

def createLoan(Currency,Balance,minimum):
    conf = helper.config.initconfig()
    avg = Poloniexapi.avgLoanOrders(Currency)
    avgmin = avg[1]
    avg = avg[0]
    completeammount = Balance
    print("Genug " + Currency + " zum Lending")
    if conf['activeLending'] == "True":
        
        if float(Balance) > minimum*4:
            ammount = completeammount/4
            create4loans(Currency,ammount,avg,avgmin)
        elif float(Balance) > minimum*3:
            ammount = completeammount/3
            create3loans(Currency,ammount,avg)
        elif float(Balance) > minimum*2:
            ammount = completeammount/2
            create2loans(Currency,ammount,avg)
        elif float(Balance) > minimum:
            create1loan(Currency,completeammount,avg)