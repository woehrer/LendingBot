# IV. Deploy the trading bot in Poloniex for One trade at a time
import sys
import helper.tradefunctions
from poloniex import Poloniex
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from time import time, sleep
import re
import helper.tradingfunctions
import CLASS.POLONIEXAPI
import logging
import CLASS.DATABASE
import helper.config
import random
import keras.models
import jsonpickle
import pickle as pk
import json

# Init Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Trading.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Init Config
conf = helper.config.initconfig()

# Init Database
Database = CLASS.DATABASE.DATABASE()
Database.connect()

# Set CurrencyPair
CurrencyPair = 'BTC_ETH'
CurrencyPair = sys.argv[1]

Period = 300
buy_signal = 0
sell_signal = 0

######################### 0 - Load necessary data, define constants
# (0) Define constants used for the trading loop
status = "Start with: Currency: " + CurrencyPair
print(status)
logging.info(status)

model = Database.requestBest(CurrencyPair)

polo = Poloniex() #TODO über die Klasse abwickeln

# (iii) According to your strategy (see article III), define a min and a max thresholds for following it (by default : 0.5 and 1)
min_threshold, max_threshold = 0.5, 1 #TODO use from Database

######################### II - One trade at a time trading bot
print('\n---------------- \n---------------- \n We start to trade {} {}, it is {}'.format(CurrencyPair, re.split('_', CurrencyPair)[0], datetime.now()))
#try:  #TODO insert try after testing
while True:
    while len(model)==0:
        # Wait for a ROI
        status = "Wait for a ROI 120s"
        print(status, end="\r")
        logging.info(status)
        sleep(120)
        model = Database.requestBest(CurrencyPair)

    if int(time())%Period < 10 :
        model = Database.requestBest(CurrencyPair)
        takeprofit = model[0]['takeprofit']
        stoploss = model[0]['stoploss']
        min_threshold = model[0]['min']
        nPCs = model[0]['nPCs']

        # (i) Load scalers and PCA
        scale_fct = pk.loads(jsonpickle.decode(model[0]['pickle']['scale_fct']))
        pca = pk.loads(jsonpickle.decode(model[0]['pickle']['pca']))
        pca_scaler = pk.loads(jsonpickle.decode(model[0]['pickle']['pca_scaler']))

        #recap = pd.read_csv('./Results/CurrencyPair{}/Period{}/Comparative_All_models_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period,stoploss, takeprofit)).sort_values('Accuracy', ascending = False)
        status = 'Best model is with {} PCs, stoploss: {}, takeprofit: {} ROI: {} Threshold: {}'.format(nPCs,stoploss,takeprofit,model[0]['ROI'],model[0]['min'])
        print(status)
        logging.info(status)

        # Load Model from Database
        clf = keras.models.model_from_json(model[0]['Model'])

        print("")
        print("It is {} ... Let's trade {} !".format(datetime.now(), CurrencyPair))
        # I- Request the data
        raw = polo.returnChartData(CurrencyPair, period = Period, start = int(time()) - Period*10000)
        df = pd.DataFrame(raw).iloc[1:] # First row may contain useless data
        df['date'] = pd.to_datetime(df["date"], unit='s')
        while (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period : #Check we've got the very recent candle : we stay here until Poloniex delivers it
            status = '{} : Waiting for actualized data... time: {}'.format(datetime.now(),(datetime.utcnow() - df['date'].iloc[-1]).total_seconds())
            logging.info(status)
            print(status,end="\r")
            sleep(random.randrange(1,10))
            raw = polo.returnChartData(CurrencyPair, period = Period, start = int(time()) - Period*10000)
            df = pd.DataFrame(raw).iloc[1:] # First row may contain useless data
            df['date'] = pd.to_datetime(df["date"], unit='s')
            if (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period*10:
                break
        if (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period*10:
            continue
        print("")
        print('We collected the recent data at {}'.format(datetime.now()))
        df = df[['close', 'date', 'high', 'low', 'open', 'volume']] # only keep required data

        # II - Compute predictions
        df = helper.tradingfunctions.compute_variables1(df)
        df_final = pd.DataFrame(pca_scaler.transform(pca.transform(scale_fct.transform(df.drop('date', 1)))))
        df_final = df_final.iloc[:, :nPCs]
        df['preds'] = (clf.predict(df_final.iloc[:, :nPCs]) > 0.5)*1
        df['proba1'] = clf.predict(df_final.iloc[:, :nPCs])

        # III - If criterion reached, we buy the asset and track the trade until we reach either the takeprofit or the stoploss
        status = 'Proba1 is {}, stoploss: {}, takeprofit: {}'.format(df['proba1'].iloc[-1],stoploss,takeprofit)
        print(status)
        logging.info(status)
        buy_signal = ((df['proba1'].iloc[-1] > min_threshold) & (df['proba1'].iloc[-1] < max_threshold))*1
        if buy_signal:
            print("BUY SIGNAL")

    print("It is {} ... We wait for the time : {} seconds to go. ".format(datetime.now(), Period - int(time())%Period), end="\r")
    sleep(5)
