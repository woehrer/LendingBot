# Flowchart

## Train

### Init Logging

### Init Config

### Init Database

### Init the Paramers

Use the Parameter for the CurrencyPair. Get it from the Arguments

### Creating subfolder

* Data
* Models
* Results

### Delete all folders at the beginning

for start with empty folders and new datas

### Init Trainer Status

* CurrencyPair
* Status
* maxcount
* count
* stoploss
* takeprofit
* nPCs

### Start to Request the CSV Data

#### Init the time

* now
* seconds in 1 month
* UNIX time for 1 month before

#### Create all folders

* Data
    * CurrencyPair
        * Period
* Models
    * CurrencyPair
        * Period
* Results
    * CurrencyPair
        * Period

#### Request loop for 72 months

* Request datas from https://poloniex.com/public?command=returnChartData&currencyPair=BTC_ETH&start=1632347521&end=1635025889&period=300
    * date
    * high
    * low
    * open
    * close
    * volume
    * quoteVolume
    * weightedAverage

    ``` {"date":1632347700,"high":0.07017342,"low":0.07008563,"open":0.07011648,"close":0.07017342,"volume":0.00246106,"quoteVolume":0.03509635,"weightedAverage":0.07012298} ```

#### Create all CSVs to one CSV

#### Save the one CSV

Poloniex.csv

#### Delete all 72 CSVs

### Start to Sort the CSV Data

#### Read the CSV

#### Convert the UNIX time into the date

#### Sort the data by date

#### Use only the important values

* close
* date
* high
* low
* open
* volume

#### Compute Cariables

* date
* close
* open
* bodysize = close - open
* high
* low
* shadowsize = high - low
* Fibonacci Numbers(3, 8, 21, 55, 144, 377)
    * sma (Simple Moving Average)
    * rsi [helper/tradingfunction.py](https://github.com/woehrer12/LendingBot/blob/108225c3474a1f965b338d2a5b42cdcaf5ca362e/LendingBot/helper/tradingfunctions.py#L21-L40)
    * min
    * max
    * volume
    * percentChange
    * RelativeSize sma = close/sma
    * diff from close
* Add modulo
    * 10
    * 100
    * 1000
    * 500
    * 50
* Add weekday
* Remove na values

#### Save to CSV

DatasetWithVariables.csv

### Init the loop counter

### Start the loop for stoploss and takeprofit

Check all options from stoploss 0.01 to 0.5 and takeprofit from 0.01 to 1

#### Compute

##### Read the CSV

DatasetWithVariables.csv

##### Compute the results

* result
    * 0 means we reached stoploss
    * 1 means we reached takeprofit
    * -1 means still in between

    Check for run into stoploss or into takeprofit

##### delete all they have no result

##### delete all they have infinity

##### Sort data by date

##### Save the CSV

DatasetWithVariablesAndY_stoploss{}_takeprofit{}.csv

#### Prepair train

##### Read the CSV

DatasetWithVariablesAndY_stoploss{}_takeprofit{}.csv

##### Search the len of the Dataframe

##### 25% to trainset

##### 50% to validationset

##### 25% to testset

##### Save trainset to CSV

TrainSet_stoploss{}_takeprofit{}.csv

##### Save validationset to CSV

ValidationSet_stoploss{}_takeprofit{}.csv

##### Save testset to CSV

TestSet_stoploss{}_takeprofit{}.csv

##### Init the Pickle Data

##### Scale the variables

##### Apply PCA

##### Scale PCA components (this accelerates training process in Deep Learning)

##### Save all in the Database

#### Train

##### Load trainset and validationset from CSV

##### EarlyStopping

##### Build model and train it

##### First Hidden Layer

##### More Hidden Layers

##### Output Layer

##### Compiling the neural network

##### Fitting the data to the training dataset

##### Save Model to File

##### Load the Pickle with the stoploss and takeprofit

##### Save the Model to Database

#### Test

##### Read validationset final from CSV

##### Read validationset from CSV

##### Load Model from Database

##### Compute predictions on testset

##### Search the accuracies and save it in the Models Database

#### Search the best

##### Init the transaction fee

##### Init the minimal number of trades in test set

##### Read validationset final from CSV

##### Read validationset from CSV

##### Compute predictions on validation_set

* Compute prdictions on testset
* preds
* proba1

##### keep only the timesteps in which the model predicts a bullish trend

##### Compute earnings loss

##### Compute recapitulative table

##### Pick the most profitable

##### Save into Testset_final and testset CSV

##### Compute predictions on testset

* Compute prdictions on testset
* preds
* proba1

