# Change Log

- Sell Agent as a Docker
- Create Docker without Cache
- split the docker-compose files
- insert a show proba script
- Remove the telegram from the Database Class
- Write a Workflow
- Search the best ROI in Trader
- Trader and Trainer Status in Web
- Split Train and Trade
- many stoploss and takeprofit tests
- Stop when not enought Currency
- wait for new start
- Check can user with TRX
- Database password and url in config
- added Docker Compose with ArangoDB and Lending
- insert web interface
- Start Trade and Trading with args [Issue 54](https://github.com/woehrer12/LendingBot/issues/54)
- Remove Data after use
- Create Lend to own Function
- struct the trading
- Can use Without Database
- transfer TRX from Lending to Exchange for pay Fee and check if enough [Issue 41](https://github.com/woehrer12/LendingBot/issues/41)
- quantil in the mean function for Lending Rate [Issue 45](https://github.com/woehrer12/LendingBot/issues/45)
- insert Trade History [Issue 33](https://github.com/woehrer12/LendingBot/issues/33)
- gitignore in Log dir
- formating Text in Telegram and Terminal 27.08.2021
- Request EUR Price and mul with the BTC
- Fix FutureWArning from mp.drop 27.08.2021
- insert exception in lending.py

### Added
   - trading.py
   - insert CSV Request [Issue 8](https://github.com/woehrer12/LendingBot/issues/8)
   - inser USDT Lending [Issue 14](https://github.com/woehrer12/LendingBot/issues/14)
### Changed
   - main.py to lending.py
### Fixed
   - OpenLoans not only BTC
   - fix style Issues in Poloniexapi.py [Issue 3](https://github.com/woehrer12/LendingBot/issues/3)
   - remove trailing spaces [Issue 2](https://github.com/woehrer12/LendingBot/issues/2)

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
